```
_________                                          
\______   \_____ _______  ______ ___________  ______
 |     ___/\__  \\_  __ \/  ___// __ \_  __ \/  ___/
 |    |     / __ \|  | \/\___ \\  ___/|  | \/\___ \ 
 |____|    (____  /__|  /____  >\___  >__|  /____  >
                \/           \/     \/           \/ 
```

## Adding a filter
- Python file must be placed in `filters` directory
- Must define following methods:
    - on_line_read
    - on_file_end (*optional*)  
- Register it by adding it to `filter_validator.py::__filters_dict`