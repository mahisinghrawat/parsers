import time


def any(_list, _lambda):
    if not isinstance(_list, list):
        raise Exception('list expected as iteratable')

    if len(_list) == 0:
        return False

    if type(_lambda(_list[0])) is not bool:
        raise Exception('Lambda must return bool')

    for x in _list:
        if _lambda(x) == True:
            return True

    return False


def tailer(file):
    f = open(file, 'r')
    line = f.readline()
    
    while True:
        line = f.readline()
        if not line:
            time.sleep(0.1)
            continue
        yield line


def parse_file(file, on_line_read, on_file_end):
    f = open(file, 'r')
    line = f.readline()
    
    while line:
        on_line_read(line)
        line = f.readline()

    f.close()
    on_file_end()


def parse_file_tailer(file, process_line_func):
    lines = tailer(file)
    for line in lines:
        process_line_func(line)
