import logging
import json
import os


class ClArg:
    def __init__(self, opt, single = True, desc = ''):
        self.opt = opt
        self.desc = desc
        self.single = single
        self.value = None
        self.values = []


    def get_instance(self):
        return ClArg(self.opt, self.single, self.desc)


    @staticmethod
    def read_config(file_location):
        if not os.path.exists(file_location):
            logging.error('~ Config file(%s) does not exists' % file_location)
            return None

        f = open(file_location, 'r')
        
        config_json = json.load(f)
        
        f.close()

        return config_json


def __parse_opt(opt_str):
    l = len(opt_str)
    if l == 0:
        return None
    
    if opt_str[0] == "-":
        if l == 1:
            logging.error('~ Invalid option string %s' % opt_str)
            return None
        if l == 2:
            return (opt_str[1], None)
        return (opt_str[1, 1], opt_str[2, l - 1])
    else:
        return (None, opt_str)


def parse_cl_args(args, registered_args):
    parsed_args = {}
    last_option = None # cl_arg instance

    for x in args:
        opt_val_pair = __parse_opt(x)
        if opt_val_pair == None:
            continue

        opt, val = opt_val_pair

        if opt and opt not in registered_args:
            logging.warning('~ Unknown option(%s)' % opt)
            continue
        
        if opt == None and val != None:
            if last_option == None:
                logging.warning('~ Value(%s) supplied, but missing option' % val)
                continue
            if last_option.single:
                last_option.value = val
                last_option = None
            else:
                last_option.values.append(val)
        elif opt != None and val == None:
            last_option = registered_args[opt].get_instance()
            parsed_args[opt] = last_option
            continue
        else:
            if last_option != None:
                last_option = None
            opt_cl_arg = None
            if opt not in parsed_args:
                opt_cl_arg = registered_args[opt].get_instance()
                parsed_args[opt] = opt_cl_arg
            else:
                opt_cl_arg = parsed_args[opt]

            if opt_cl_arg.single:
                opt_cl_arg.value = val
            else:
                opt_cl_arg.values.append(val)

    return parsed_args