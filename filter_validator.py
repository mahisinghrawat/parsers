import inspect
import logging

import filters.process_hv_alter_trans_t
import filters.stable_ose_price


__filters_dict = {
    "process_hv_alter_trans_t": filters.process_hv_alter_trans_t,
    "stable_ose_price": filters.stable_ose_price
}


# filter is valid if it has method on_line_read (which log line(string) as argument, optional)
# on_file_end
def __validate_filter(filter):
    required_methods = {'on_line_read', 'on_file_end'}

    for method_name in inspect.getmembers(filter, inspect.ismethod):
            if method_name in required_methods:
                required_methods.remove(method_name)
                if len(required_methods == 0):
                    return True

    return False


def try_get_filter(config_json):
    global __filters_dict

    filter_name = config_json.get('filter', None)

    if filter_name == None:
        logging.error('Missing \'filter\' key in config')
        return None

    filter = __filters_dict.get(filter_name, None)

    if filter == None:
        logging.error('Unknown filter(%s)' % filter_name)
        return None

    if not __validate_filter(filter):
        logging.error('Invalid filter(%s)' % filter_name)
        return None

    return filter