import re


__data_by_order_id = dict()


def filter(log_line):
    global __data_by_order_id

    regex = re.compile(r'\[Process_hv_alter_trans_t\] order_id\((\d+)\) price\((\d+)\) volume\((\d+)\)')
    match = regex.search(log_line)
    if match:
        order_id = match.group(1)
        price = match.group(2)
        volume = match.group(3)

        if order_id not in __data_by_order_id:
            __data_by_order_id[order_id] = list()
        
        # list of dict/events
        order = __data_by_order_id[order_id]

        order.append(
            {
                "price": price,
                "volume": volume
            }
        )


def filter_print():
    global __data_by_order_id

    for order_id, order_events in __data_by_order_id.items():
        print('Order_id %s', order_id)
        last_volume = int(order_events[0]['volume'])
        for order_event in order_events:
            current_vol = int(order_event['volume'])

            price_change_symbol = '='
            if current_vol > last_volume:
                price_change_symbol = '+'
            elif current_vol < last_volume:
                price_change_symbol = '-'

            print('\tprice: %s\tvolume: %s %s' % (order_event['price'], price_change_symbol, current_vol))

            last_volume = current_vol

        # print new line at the end
        print('')