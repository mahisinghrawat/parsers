import re


class InstrumentInfo:
    def __init__(self, instr, price, breaker = False):
        self.instr = instr
        self.price = price
        self.breaker_count = 0

        if breaker:
            self.breaker_count += 1

    def increment_breaker(self):
        self.breaker_count += 1


# first group instrument_id(market:feedcode)
# second group ref_price
static_price_broadcast_rgx = re.compile(r'HandlePriceLimitsMessage[^\n]*instrument\(([\S]*)\)[^\n]*ref_price(\(\d*\))')

dynamic_circuit_breaker_rgx = re.compile(r'HandlePriceLimitsMessage\] Dynamic [^\n]*instrument\(([\S]*)\)[^\n]*ref_price(\(\d*\))')

__instruments = list()


def __print_top_instruments():
    global __instruments

    for i in range(0, min([len(__instruments), 10])):
        print('instr(%s) price(%s)' % (__instruments[i].instr, __instruments[i].price))
    if len(__instruments) == 0:
        print('NULL')
    print('-------------------------------')
    

def filter(log_line):
    global __instruments

    match = dynamic_circuit_breaker_rgx.search(log_line)
    
    is_circuit_breaker = True
    if not match:
        is_circuit_breaker = False
        match = static_price_broadcast_rgx.search(log_line)    

    # do not care
    if not match:
        return

    instr_info = InstrumentInfo(match.group(1), match.group(2), is_circuit_breaker)

    already_exists = False

    for instrument in __instruments:
        if instrument.instr == instr_info.instr:
            instrument.increment_breaker()
            already_exists = True
            break
    
    if not already_exists:
        __instruments.append(instr_info)

    __instruments = sorted(__instruments, key = lambda instr_info: instr_info.breaker_count)

    __print_top_instruments()


def end_of_file():
    return