#!/usr/bin/python

import cl_args_parser as cl_parser
import filter_validator
import helper

import logging
import os
import sys


def main():
    registered_opts = {
        'f': cl_parser.ClArg('f', True, 'File to parse'),
        't': cl_parser.ClArg('t', True, 'Tail file')
    }

    # parse registered options
    option = cl_parser.parse_cl_args(sys.argv[1:], registered_opts)

    if 'f' not in option:
        logging.error('~ Missing file(-f) option.')
        exit()

    # do tail file?
    tail_file = False
    if 't' in option and option['t'].value == 'True':
        tail_file = True

    log = option['f'].value

    # check if file exists
    if not os.path.exists(log):
        logging.error('~ Log(%s) does not exists' % log)
        exit()

    # read config
    config_filename = 'config.json'
    config_json = cl_parser.ClArg.read_config(config_filename)
    if config_json == None:
        exit()

    # get filter
    filter = filter_validator.try_get_filter(config_json)
    if filter == None:
        exit()

    # tail file
    if tail_file:
        helper.parse_file_tailer(log, filter.filter)
        exit()

    #read file
    helper.parse_file(log, filter.filter, filter.end_of_file)


if __name__ == '__main__':
    main()